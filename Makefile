# Uncomment lines below if you have problems with $PATH
SHELL := /usr/local/bin/zsh
#PATH := /usr/local/bin:$(PATH)

DEVICE=/dev/cu.usbmodem1414201

all:
	platformio -c vim run

format:
	clang-format -style=file -i src/*

upload:
	platformio -c vim run --target upload --upload-port ${DEVICE}

clean:
	platformio -c vim run --target clean

program:
	platformio -c vim run --target program

uploadfs:
	platformio -c vim run --target uploadfs

update:
	platformio -c vim update

monitor:
	platformio device monitor -b 115200 -p ${DEVICE}

.PHONY: clear
clear:
	echo -n -e 'a\x00\x00\x00' | mosquitto_pub -p 1883 -h broker.emqx.io -t 'LuxPopuliOmnibus/device/command' -s
