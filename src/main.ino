#define USE_AIRLIFT

#include <Adafruit_LIS3DH.h> // For accelerometer
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <Adafruit_Protomatter.h> // For RGB matrix
#include <Base64.h>
#include <Fonts/FreeSans9pt7b.h> // Large friendly font
#include <WiFiNINA.h>
#include <Wire.h> // For I2C communication

#include "arduino_secrets.h"
#include "logo.h"
#include "heart.h"
#include "+1.h"
#include "hankey.h"
#include "joy.h"
#include "slightly_frowning_face.h"
#include "slightly_smiling_face.h"
#include "sparkles.h"

// Adjust this number for the sensitivity of the 'click' force
// this strongly depend on the range! for 16G, try 5-10
// for 8G, try 10-20. for 4G try 20-40. for 2G try 40-80
#define CLICKTHRESHHOLD 80

// RGB MATRIX (PROTOMATTER) LIBRARY STUFF ----------------------------------

#define HEIGHT 16 // Matrix height (pixels) - SET TO 64 FOR 64x64 MATRIX!
#define WIDTH 32 // Matrix width (pixels)
#define BUTTON_UP 2
#define BUTTON_DOWN 3

#define COMMAND_TOPIC "LuxPopuliOmnibus/device/command"
#define TAP_TOPIC "LuxPopuliOmnibus/device/tap"

struct __attribute__((__packed__)) RGB {
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
};

struct __attribute__((__packed__)) XYRGB {
  uint8_t x = 0;
  uint8_t y = 0;
  struct RGB rgb;
};

struct __attribute__((__packed__)) Frame {
  uint16_t data[HEIGHT * WIDTH] = {0};
};

struct __attribute__((__packed__)) Emoji {
  char name[32] = {0};
};

struct __attribute__((__packed__)) Packet {
  char command = ' ';
  union Paramaters {
    struct RGB rgb;
    struct XYRGB xyrgb;
    struct Frame frame;
    struct Emoji emoji;
  } parameters;
};

union PacketBuffer {
  struct Packet packet;
  uint8_t buffer[sizeof(struct Packet)];
};

uint8_t rgbPins[] = {7, 8, 9, 10, 11, 12};
uint8_t addrPins[] = {17, 18, 19};
uint8_t clockPin = 14;
uint8_t latchPin = 15;
uint8_t oePin = 16;

Adafruit_Protomatter matrix(WIDTH, 4, 1, rgbPins, sizeof(addrPins), addrPins, clockPin, latchPin, oePin, false);

WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_SERVERPORT, MQTT_USERNAME, MQTT_PW);
Adafruit_MQTT_Subscribe commandTopic = Adafruit_MQTT_Subscribe(&mqtt, COMMAND_TOPIC);
Adafruit_MQTT_Publish tapTopic = Adafruit_MQTT_Publish(&mqtt, TAP_TOPIC);

Adafruit_LIS3DH accel = Adafruit_LIS3DH();

long previousMillis = 0;
long interval = 1000;

char ssid[] = WIFI_SSID; // your network SSID (name)
char pass[] = WIFI_PASS; // your network password (use for WPA, or use as key for WEP)
int wstatus = WL_IDLE_STATUS; // the Wifi radio's status

int16_t textX = 0;
int16_t textY = 0;
char str[50]; // Buffer to hold scrolling message text

void err(int x) {
  uint8_t i;
  pinMode(LED_BUILTIN, OUTPUT); // Using onboard LED
  for (i = 1;; i++) { // Loop forever...
    digitalWrite(LED_BUILTIN, i & 1); // LED on/off blink to alert user
    delay(x);
  }
}

// SETUP - RUNS ONCE AT PROGRAM START --------------------------------------

void setup(void) {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_UP, INPUT_PULLUP);
  pinMode(BUTTON_DOWN, INPUT_PULLUP);

  Serial.begin(115200);
  // while (!Serial) delay(10);

  ProtomatterStatus status = matrix.begin();
  Serial.printf("Protomatter begin() status: %d\n", status);

  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    err(1500);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  // attempt to connect to Wifi network:
  while (wstatus != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network:
    wstatus = WiFi.begin(ssid, pass);

    delay(1000);
  }

  if (!accel.begin(0x19)) {
    Serial.println("Couldn't find accelerometer");
    err(250); // Fast bink = I2C error
  }
  accel.setRange(LIS3DH_RANGE_2_G); // 2, 4, 8 or 16 G!
  accel.setClick(1, CLICKTHRESHHOLD);

  commandTopic.setCallback(commandCallback);
  mqtt.subscribe(&commandTopic);
}

// MAIN LOOP - RUNS ONCE PER FRAME OF ANIMATION ----------------------------

bool heartbeat = false;
void loop() {
  if (digitalRead(BUTTON_UP) == LOW) {
    // TODO: send mqtt message (command?) to sync web clients
    Serial.println(F("UP BUTTON."));
    matrix.fillScreen(0); // Clear screen
    matrix.show();
    while (digitalRead(BUTTON_UP) == LOW)
      ; // Wait for release
  }
  if (digitalRead(BUTTON_DOWN) == LOW) {
    // TODO: send mqtt message (command?) to sync web clients
    Serial.println(F("DOWN BUTTON."));
    showEmoji("heart");
    while (digitalRead(BUTTON_DOWN) == LOW)
      ; // Wait for release
  }
  tapCheck();

  MQTT_connect();
  mqtt.processPackets(1000);

  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    if(mqtt.ping()) {
      if (heartbeat){
        Serial.print(F("ba-"));
        matrix.drawPixel(0, 0, matrix.color565(0xff, 0x00, 0x33));
        matrix.show();
      } else {
        Serial.println(F("dum"));
        matrix.drawPixel(0, 0, matrix.color565(0, 0, 0));
        matrix.show();
      }
      heartbeat = !heartbeat;
    } else {
      showEmoji("hankey");
      mqtt.disconnect();
    }
  }
}

void showLogo() {
  showEmoji("logo");
}

void showBmp(uint8_t *data) {
  matrix.fillScreen(0);
  matrix.show();

  uint32_t offset = data[10];
  uint16_t *bitmap = (uint16_t *)(data + offset);

  matrix.drawRGBBitmap(0, 0, bitmap, WIDTH, HEIGHT);
  matrix.show();
}

void showEmoji(const char *name) {
  if (strcmp("logo", name) == 0) {
    showBmp(logo_bmp);
  } else if (strcmp("heart", name) == 0) {
    showBmp(heart_bmp);
  } else if (strcmp("+1", name) == 0) {
    showBmp(_1_bmp);
  } else if (strcmp("hankey", name) == 0) {
    showBmp(hankey_bmp);
  } else if (strcmp("joy", name) == 0) {
    showBmp(joy_bmp);
  } else if (strcmp("slightly_smiling_face", name) == 0) {
    showBmp(slightly_smiling_face_bmp);
  } else if (strcmp("slightly_frowning_face", name) == 0) {
    showBmp(slightly_frowning_face_bmp);
  } else if (strcmp("sparkles", name) == 0) {
    showBmp(sparkles_bmp);
  } else {
    Serial.printf("No emoji matached for %s\n", name);
  }
}

void tapCheck() {
  uint8_t click = accel.getClick();
  uint8_t tap[1] = {0};
  if (click == 0)
    return;
  if (!(click & 0x30))
    return;
  Serial.printf("Click detected (%x): ", click);
  // TODO: Throttle to prevent hitting adafruit io limit
  if (click & 0x10) {
    Serial.print("single click\n");
    tap[0] = 1;
  }
  if (click & 0x20) {
    Serial.print("double click\n");
    tap[0] = 2;
  }

  if (tap[0] > 0) {
    if (!tapTopic.publish(tap, sizeof(tap))) {
      Serial.println(F("Tap Publish Failed."));
    }
  }
}

void printHex(const uint8_t *data, const uint32_t numBytes) {
  uint32_t szPos;
  for (szPos = 0; szPos < numBytes; szPos++) {
    Serial.print(F("0x"));
    // Append leading 0 for small values
    if (data[szPos] <= 0xF) {
      Serial.print(F("0"));
      Serial.print(data[szPos] & 0xf, HEX);
    } else {
      Serial.print(data[szPos] & 0xff, HEX);
    }
    // Add a trailing space if appropriate
    if ((numBytes > 1) && (szPos != numBytes - 1)) {
      Serial.print(F(" "));
    }
  }
  Serial.println();
}

void commandCallback(char *data, uint16_t len) {
  parseMessage((uint8_t*)data, len);
}

void parseMessage(uint8_t *buffer, uint32_t length) {
  PacketBuffer pb = {};
  uint16_t x, y, color;
  RGB rgb;

  Serial.print("parseMessage: ");
  printHex(buffer, length);

  for (unsigned int i = 0; i < length; i++) {
    pb.buffer[i] = buffer[i];
  }
  Packet packet = pb.packet;

  switch (packet.command) {
  case 'a':
    rgb = packet.parameters.rgb;
    color = matrix.color565(rgb.r, rgb.g, rgb.b);
    matrix.fillScreen(color);
    break;
  case 'c':
    x = packet.parameters.xyrgb.x;
    y = packet.parameters.xyrgb.y;
    rgb = packet.parameters.xyrgb.rgb;
    color = matrix.color565(rgb.r, rgb.g, rgb.b);
    matrix.drawPixel(x, y, color);
    break;
  case 'C':
    x = packet.parameters.xyrgb.x;
    y = packet.parameters.xyrgb.y;
    color = matrix.getPixel(x, y);
    //TODO: Send response
    break;
  case 's':
    showEmoji(packet.parameters.emoji.name);
    break;
  case 'f':
    break;
  case '!':
    Serial.println("Test command");
    break;
  }

  matrix.show();
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print(F("Connecting to MQTT... "));

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println(F("Retrying MQTT connection in 5 seconds..."));
    matrix.fillScreen(0); // Clear screen
    matrix.setTextColor(matrix.color565(0xff, 0x00, 0x33));
    matrix.setCursor(textX, textY);
    matrix.print(retries);
    matrix.show();

    mqtt.disconnect();
    delay(5000);
    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      while (true) {
        matrix.fillScreen(0); // Clear screen
        matrix.setTextColor(matrix.color565(0xff, 0x00, 0x33));
        matrix.setCursor(textX, textY);
        matrix.print("RESET");
        matrix.show();
      }
    }
  }
  showLogo();
  Serial.println(F("MQTT Connected!"));
}
