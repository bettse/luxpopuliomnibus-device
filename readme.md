

### device protocol

Inspired by the blink1 hid protocol

Lowercase are commands, uppercase are requests for response

#### Set pixel to color

| position | length | description     | example |
| -------- | ------ | --------------- | ------- |
| 0        | 1      | literal 'c'     | `c`     |
| 1        | 1      | x cooridinate   | 13      |
| 2        | 1      | y cooridinate   | 9       |
| 3        | 1      | red component   | 0xff    |
| 4        | 1      | green component | 0xff    |
| 5        | 1      | blue component  | 0xff    |

#### Set all to color

| position | length | description     | example |
| -------- | ------ | --------------- | ------- |
| 0        | 1      | literal 'a'     | `a`     |
| 1        | 1      | red component   | 0xff    |
| 2        | 1      | green component | 0xff    |
| 3        | 1      | blue component  | 0xff    |

#### Set frame

f [RGB565] x 512

#### Request pixel color

C II XX YY

II: request id to associate response with this request

#### Request all (full frame/all pixels)

A II

#### Display text

t <text>

font size/wrapping/scrolling?


### Testing

`echo -n 'c\x17\x09\x00\xb1\x9a' | mosquitto_pub -p 1883 -h broker.hivemq.com -t 'LuxPopuliOmnibus/device/command' -s`

### Converting png to RGB565 byte array

- convert ../logo.png -resize 32x16 -background black -gravity center -extent 32x16 -type truecolor -define bmp:subtype=RGB565 bmp2:logo.bmp
- xxd -c 16 -i logo.bmp > src/logo.h
